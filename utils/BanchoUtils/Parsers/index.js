module.exports = {
	LoginParser: require('./LoginParser'),
	StatusParser: require('./StatusParser'),
	SpectateParser: require('./SpectateParser'),
	ChatChannelParser: require('./ChatChannelParser'),
	ChatMessageParser: require('./ChatMessageParser'),
	UserParser: require('./UserParser'),
	MatchInformation: require('./MatchInformation'),
	MatchJoinData: require('./MatchJoinData'),
	ScoreFrame: require('./ScoreFrame')
};
